App.info({
  id:          "io.gugo.app",
  name:        "GUGO",
  description: "GUGO",
  author:      "GUGO",
  email:       "contact@gugo.io",
  website:     "http://www.gugo.io",
  version:     "0.0.51"
});